from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime

class GratuityPolicy(models.Model):
    _name = 'gratuity.policy'
    _description = "Gratuity Policy details"
    _inherit = ['mail.thread']
    _rec_name = 'reference'
    _order = "range_start asc"

    reference = fields.Char('Reference', required=True, copy=False, track_visibility='onchange', readonly=True, default=lambda self: _('New'))
    range_start = fields.Integer(required=True, track_visibility='onchange')
    range_end = fields.Integer(required=True, track_visibility='onchange')
    basic_count = fields.Float(required=True, track_visibility='onchange')
    state = fields.Selection(selection=[('draft', 'Drafted'), ('running', 'Running'), ('cancel', 'Canceled')], group_expand='_expand_states', default='draft', track_visibility='onchange')

    status_show = fields.Selection([
        ('draft', 'Drafted at '),
        ('running', 'Ran at '),
        ('cancel', 'Cancelled at '),
    ], string='State Show', default='draft', readonly="True")
    date_show = fields.Datetime(string='Date show', default=fields.Datetime.now(), readonly="True")

    def _expand_states(self, states, domain, order):
        return [key for key, val in type(self).state.selection]

    @api.model
    def create(self, vals):
        if vals['range_start'] >= vals['range_end']:
            raise ValidationError('Range start should be lower than range end')

        policy_array = self.env['gratuity.policy'].search_read([], ['range_start', 'range_end'])
        maximum_range = max([r['range_end'] for r in policy_array])

        if not vals['range_start'] > maximum_range:
            for policy in policy_array:
                if policy['range_start'] <= vals['range_start'] <= policy['range_end'] or policy['range_start'] <= vals['range_end'] <= policy['range_end']:
                    raise UserError('This range is already available!')
                if policy['range_start'] > vals['range_start'] and vals['range_end'] > policy['range_end']:
                    raise UserError('This range is already available!')

        if vals.get('reference', _('New')) == _('New'):
            vals['reference'] = self.env['ir.sequence'].next_by_code('gratuity.policy') or _('New')
        res = super(GratuityPolicy, self).create(vals)
        return res

    @api.multi
    def unlink(self):
        for record in self:
            if record.state not in ['draft', 'cancel']:
                raise ValidationError("Only draft and canceled policies can be deleted.")
        return super(GratuityPolicy, self).unlink()

    def action_do_draft(self):
        for record in self:
            record.state = "draft"
            record.status_show = "draft"
            record.date_show = datetime.now()
        return True

    def action_do_running(self):
        for record in self:
            record.state = "running"
            record.status_show = "running"
            record.date_show = datetime.now()
        return True

    def action_do_cancel(self):
        for record in self:
            record.state = "cancel"
            record.status_show = "cancel"
            record.date_show = datetime.now()
        return True

