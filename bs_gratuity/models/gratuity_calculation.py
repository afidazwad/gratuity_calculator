from odoo import models, fields, api
from datetime import date, datetime
from dateutil import relativedelta
from odoo.exceptions import UserError, Warning

class GratuityCalculation(models.TransientModel):
    _name = 'gratuity.calculation'
    _description = "Gratuity Calculation Details"

    emp_categ_list = [('permanent', 'Permanent'),
                      ('contractual', 'Contractual'),
                      ('casual', 'Casual'),
                      ('part_time', 'Part Time'),
                      ('probation', 'Probation')]

    employee_id = fields.Many2one(comodel_name='hr.employee', string='Employee',
                                  default=lambda self: self.env['hr.employee'].search(
                                      [('user_id', '=', self.env.user.id)], limit=1).id)

    employee_name = fields.Char(compute='_compute_details', readonly="True")
    designation = fields.Char(compute='_compute_details', readonly="True")
    employee_category = fields.Selection(selection=emp_categ_list, readonly="True", string='Employee Category',compute='_compute_details', )
    joining_date = fields.Date('Date of Joining',compute='_compute_details', readonly="True")
    confirm_date = fields.Date(compute='_compute_details', readonly="True")

    expected_confirm_date = fields.Date()
    expect_basic = fields.Float()
    expect_leave_year = fields.Date()
    total_gratuity = fields.Float()

    @api.depends('employee_id', 'employee_name','employee_category','joining_date', 'confirm_date')
    def _compute_details(self):
        if self.employee_id:
            self.employee_name = self.employee_id.name
            self.designation = self.employee_id.job_id.name
            self.employee_category = self.employee_id.employee_categ
            self.joining_date = self.employee_id.date_join
            self.confirm_date = self.employee_id.date_confirm

    @api.onchange('expect_leave_year')
    def _onchange_expect_leave_year(self):
        if self.expect_leave_year:
            expected_year = datetime.strptime(self.expect_leave_year, "%Y-%m-%d")

            if expected_year <= datetime.today():
                raise Warning('Expected job leaving year should be equal or higher than present date %s' % date.today())

    @api.onchange("expect_basic")
    def _onchange_expect_basic(self):
        if self.expect_basic:
            if self.expect_basic < 0:
                raise UserError('Expected basic amount should be positive.')

    @api.multi
    def calculate_gratuity(self):
        if not self.env['gratuity.policy'].search([('state', '=', 'running')]):
            raise Warning('No running policy for gratuity yet!')
        else:
            range_available = False
            if self.expected_confirm_date:
                if self.expected_confirm_date < self.joining_date:
                    raise UserError('Expected confirm date should be higher than the date of joining.')
                else:
                    self.confirm_date = self.expected_confirm_date

            leave_year = datetime.strptime(self.expect_leave_year, "%Y-%m-%d")
            confirm_year = datetime.strptime(self.confirm_date, "%Y-%m-%d")

            total_job_period = relativedelta.relativedelta(leave_year, confirm_year).years

            applicable_llist = []
            if self.env['ir.config_parameter'].get_param('bs_gratuity.permanent'):
                applicable_llist.append('permanent')
            if self.env['ir.config_parameter'].get_param('bs_gratuity.contractual'):
                applicable_llist.append('contractual')
            if self.env['ir.config_parameter'].get_param('bs_gratuity.casual'):
                applicable_llist.append('casual')
            if self.env['ir.config_parameter'].get_param('bs_gratuity.part_time'):
                applicable_llist.append('part_time')
            if self.env['ir.config_parameter'].get_param('bs_gratuity.probation'):
                applicable_llist.append('probation')

            if not applicable_llist:
                raise Warning('No employee category is selected yet for gratuity applicability.')

            if self.employee_category not in applicable_llist:
                raise UserError('You are not applicable for gratuity !')

            policy_array = self.env['gratuity.policy'].search_read([], ['range_start', 'range_end', 'basic_count'])

            maximum_range = max([r['range_end'] for r in policy_array])
            maximum_basic = max([r['basic_count'] for r in policy_array])

        if self.env['ir.config_parameter'].get_param('bs_gratuity.active_option'):
            start = self.env['ir.config_parameter'].get_param('bs_gratuity.gratuity_start')
            disburse = self.env['ir.config_parameter'].get_param('bs_gratuity.gratuity_disburse')

            start_year = datetime.strptime(start, "%Y-%m-%d")
            disburse_year = datetime.strptime(disburse, "%Y-%m-%d")

            expected_year = datetime.strptime(self.expect_leave_year, "%Y-%m-%d")

            if expected_year < start_year or expected_year < disburse_year:
                raise Warning('Your expected job leaving year is not valid for gratuity')

            gratuity_valid_years = relativedelta.relativedelta(leave_year, start_year).years

            for values in policy_array:
                if total_job_period >= values['range_start'] and total_job_period <= values['range_end']:
                    self.total_gratuity = self.expect_basic * gratuity_valid_years * values['basic_count']
                    range_available = True
            if not range_available:
                raise Warning('Your total job duration is not valid for the running policies.')
            return True

        else:
            for record in self:
                if leave_year < confirm_year:
                    raise UserError('Job leaving year should be higher than job confirmation date %s' % record.confirm_date)

                if total_job_period > maximum_range:
                    record.total_gratuity = record.expect_basic * total_job_period * maximum_basic
                    break

                for values in policy_array:
                    if total_job_period >= values['range_start'] and total_job_period <= values['range_end']:
                        record.total_gratuity = record.expect_basic * total_job_period * values['basic_count']
                        range_available = True
                if not range_available:
                    raise Warning('Your total job duration is not valid for the running policies.')
        return True



