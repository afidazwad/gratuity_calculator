from odoo import fields, models, api
from datetime import date, datetime
from odoo.exceptions import UserError

class ResConfigSettings(models.TransientModel):
    _inherit = ['res.config.settings']

    emp_categ_list = [('permanent', 'Permanent'),
                      ('contractual', 'Contractual'),
                      ('casual', 'Casual'),
                      ('part_time', 'Part Time'),
                      ('probation', 'Probation')]

    permanent = fields.Boolean('Permanent')
    contractual = fields.Boolean('Contractual')
    casual = fields.Boolean('Casual')
    part_time = fields.Boolean('Part Time')
    probation = fields.Boolean('Probation')

    active_option = fields.Boolean()
    gratuity_start_year = fields.Date()
    gratuity_disburse_year = fields.Date()

    @api.onchange('gratuity_start_year')
    def _onchange_gratuity_start_year(self):
        if self.gratuity_start_year:
            start_year = datetime.strptime(self.gratuity_start_year, "%Y-%m-%d")
            if start_year <= datetime.today():
                return {
                    'warning': {'title': 'Date Error!', 'message': 'Gratuity start date should be equal or higher than present date %s' % date.today()},
                    'value': {
                        'gratuity_start_year': None,
                    }
                }

    @api.onchange('gratuity_disburse_year')
    def _onchange_gratuity_disburse_year(self):
        if self.gratuity_disburse_year:
            if not self.gratuity_start_year:
                return {
                    'warning': {'title': 'Warning!', 'message': 'Please provide the gratuity start date first'},
                    'value': {
                        'gratuity_disburse_year': None,
                    }
                }

            start_year = datetime.strptime(self.gratuity_start_year, "%Y-%m-%d")
            disburse_year = datetime.strptime(self.gratuity_disburse_year, "%Y-%m-%d")
            if disburse_year < start_year:
                self.gratuity_start_year = False
                raise UserError('Gratuity disburse date should be equal or higher than gratuity start date %s' % self.gratuity_start_year)


    def set_values(self):
        super(ResConfigSettings, self).set_values()
        Param = self.env['ir.config_parameter']
        Param.set_param("bs_gratuity.permanent", self.permanent)
        Param.set_param("bs_gratuity.contractual", self.contractual)
        Param.set_param("bs_gratuity.casual", self.casual)
        Param.set_param("bs_gratuity.part_time", self.part_time)
        Param.set_param("bs_gratuity.probation", self.probation)

        Param.set_param("bs_gratuity.active_option", self.active_option)
        Param.set_param("bs_gratuity.gratuity_start", self.gratuity_start_year)
        Param.set_param("bs_gratuity.gratuity_disburse", self.gratuity_disburse_year)

    @api.model
    def get_values(self):
        values = super(ResConfigSettings, self).get_values()
        params = self.env['ir.config_parameter']
        values.update(permanent=params.get_param('bs_gratuity.permanent'))
        values.update(contractual=params.get_param('bs_gratuity.contractual'))
        values.update(casual=params.get_param('bs_gratuity.casual'))
        values.update(part_time=params.get_param('bs_gratuity.part_time'))
        values.update(probation=params.get_param('bs_gratuity.probation'))
        values.update(active_option=params.get_param('bs_gratuity.active_option'))
        values.update(gratuity_start_year=params.get_param('bs_gratuity.gratuity_start'))
        values.update(gratuity_disburse_year=params.get_param('bs_gratuity.gratuity_disburse'))
        return values