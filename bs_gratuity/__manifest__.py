# -*- coding: utf-8 -*-
{
    'name': "Gratuity Calculator",

    'author': "Brain Station 23 LTD",
    'website': "http://www.brainstation-23.com",
    'license': "OPL-1",

    'category': 'Extra Tools',
    'depends': ['base', 'hr', 'mail'],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/gratuity_calculation_views.xml',
        'views/gratuity_calculation_menu.xml',
        'views/gratuity_policy_views.xml',
        'views/gratuity_policy_menu.xml',
        'views/res_config_settings_views.xml',
    ],

    'application': True,
    'installable': True,
}